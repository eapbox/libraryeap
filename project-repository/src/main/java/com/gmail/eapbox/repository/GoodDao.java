package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.Good;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface GoodDao extends GenericDao<Good, Integer> {

    Good getByInvNumber(Integer invNumber);

}
