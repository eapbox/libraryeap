package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.UserInformationDao;
import com.gmail.eapbox.repository.model.UserInformation;
import org.springframework.stereotype.Repository;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Repository
public class UserInformationDaoImpl
    extends GenericDaoImpl<UserInformation, Integer>
    implements UserInformationDao {

}
