package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.UserDao;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.repository.model.UserRole;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Properties;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Repository
public class UserDaoImpl
    extends GenericDaoImpl<User, Integer>
    implements UserDao {

    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    @Autowired
    private Properties properties;

    @Override
    public User getUserByEmail(String email) {
        logger.info("getUserByEmail: " + email);

        String hql = properties.getProperty("select.all.from.user.where.username");
        Query query = getSession().createQuery(hql);
        query.setParameter("email", email);
        Object uniqueResult = query.uniqueResult();
        return uniqueResult != null ? (User) uniqueResult : null;
    }

    @Override
    public void changeBlocked(Integer id, Boolean blocked) {
        logger.info("changeBlocked: " + id);

        String hql = properties.getProperty("update.user.set.blocked.where.id");
        Query query = getSession().createQuery(hql);
        query.setParameter(0, blocked);
        query.setParameter(1, id);
        int res = query.executeUpdate();
    }

    @Override
    public void changeRole(Integer id, UserRole role) {
        logger.info("changeRole: " + id);

        String hql = properties.getProperty("update.user.set.role.where.id");
        Query query = getSession().createQuery(hql);
        query.setParameter(0, role);
        query.setParameter(1, id);
        int res = query.executeUpdate();
    }
}
