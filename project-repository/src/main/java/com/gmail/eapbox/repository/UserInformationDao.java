package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.UserInformation;

import java.io.Serializable;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface UserInformationDao extends GenericDao<UserInformation, Integer>, Serializable {
}
