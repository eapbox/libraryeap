package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.FileDao;
import com.gmail.eapbox.repository.GenericDao;
import com.gmail.eapbox.repository.model.FileEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Repository
public class FileDaoImpl
    extends GenericDaoImpl <FileEntity, Integer>
    implements FileDao {

}
