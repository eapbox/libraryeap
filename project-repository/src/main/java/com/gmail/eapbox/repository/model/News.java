package com.gmail.eapbox.repository.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Entity
@Table(name = "T_NEWS")
public class News implements Serializable {
    private static final long serialVersionUID = 104053852725530374L;

    @Id
    @Column(name = "F_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "F_CAPTION")
    private String caption;

    @Column(name = "F_CONTENT")
    private String content;

    @Column(name = "F_DATE")
    private String date;

    @OneToOne(mappedBy = "news", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private FileEntity fileEntity;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(id, news.id) &&
            Objects.equals(caption, news.caption);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, caption);
    }
}
