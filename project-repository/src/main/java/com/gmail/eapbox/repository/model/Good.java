package com.gmail.eapbox.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Entity
@Table(name = "T_GOOD")
public class Good implements Serializable {
    private static final long serialVersionUID = 7573932222994562205L;

    @Id
    @Column(name = "F_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "F_NAME")
    private String name;

    @Column(name = "F_INVNUMBER")
    private Integer invNumber;

    @Column(name = "F_INFO")
    private String info;

    @Column(name = "F_PRICE")
    private BigDecimal price;

    @Column(name = "F_BLOCKED")
    private Boolean isBlocked;

    @OneToMany(mappedBy="good", cascade = CascadeType.ALL)
    private List<Order> orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInvNumber() {
        return invNumber;
    }

    public void setInvNumber(Integer invNumber) {
        this.invNumber = invNumber;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Good good = (Good) o;
        return Objects.equals(id, good.id) &&
            Objects.equals(invNumber, good.invNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, invNumber);
    }
}
