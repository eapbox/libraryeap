package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.GoodDao;
import com.gmail.eapbox.repository.model.Good;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;

import java.util.Properties;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Repository
public class GoodDaoImpl
    extends GenericDaoImpl<Good, Integer>
    implements GoodDao {

    private static final Logger logger = Logger.getLogger(GoodDaoImpl.class);

    @Autowired
    private Properties properties;

    @Override
    public Good getByInvNumber(Integer invNumber) {
        logger.info("getByInvNumber: " + invNumber);

        String hql = properties.getProperty("select.all.from.good.where.invNumber");
        Query query = getSession().createQuery(hql);
        query.setParameter("invNumber", invNumber);
        Object uniqueResult = query.uniqueResult();
        return uniqueResult != null ? (Good) uniqueResult : null;
    }
}
