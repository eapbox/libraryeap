package com.gmail.eapbox.repository.model;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public enum UserRole {
    ROLE_ROOT,
    ROLE_ADMIN,
    ROLE_USER
}
