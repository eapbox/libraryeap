package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.Good;
import com.gmail.eapbox.repository.model.Order;
import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.repository.model.User;

import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface OrderDao extends GenericDao<Order, Integer>{
    List<Order> getBasket(String username);

    Order getOrderBasketByGood(Good good);

    List<Order> getUserOrders(String username);

    List<Order> getAllOrders();

    void setAmountGood(Integer id, Integer amount);

    void setStatus(OrderStatus status, User user);

    void changeOrderStatus(Integer orderNumber, OrderStatus orderStatus);

    void deleteById(Integer id);

    Integer getCurrentOrderNumber(String username);
}
