package com.gmail.eapbox.repository;


import com.gmail.eapbox.repository.model.FileEntity;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface FileDao extends GenericDao<FileEntity, Integer> {
}
