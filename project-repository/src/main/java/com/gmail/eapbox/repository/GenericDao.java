package com.gmail.eapbox.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface GenericDao<T extends Serializable, ID extends Serializable> {

    ID save(T entity);
    void saveOrUpdate(T entity);
    void delete(T entity);
    void deleteAll();
    List<T> findAll();
    T findById(ID id);

}
