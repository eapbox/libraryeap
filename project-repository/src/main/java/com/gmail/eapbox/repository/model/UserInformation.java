package com.gmail.eapbox.repository.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Entity
@Table(name = "T_USER_INFORMATION")
public class UserInformation implements Serializable {
    private static final long serialVersionUID = -1917184521328648854L;

    @Id
    @Column(name = "F_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "F_NAME")
    private String name;

    @Column(name = "F_SURNAME")
    private String surname;

    @Column(name = "F_FATHERNAME")
    private String fathername;

    @Column(name = "F_PHONE_NUMBER")
    private String phone;

    @Column(name = "F_ADDRESS")
    private String address;

    @Column(name = "F_INFO")
    private String addInfo;

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phoneNumber) {
        this.phone = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
