package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.GenericDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable> implements GenericDao<T, ID> {

    @Autowired
    private SessionFactory sessionFactory;

    private final Class<T> entityClass;

    public GenericDaoImpl() {
        this.entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public T findById(final ID id) {
        return (T) getSession().get(this.entityClass, id);
    }

    @Override
    public ID save(T entity) {
        return (ID) getSession().save(entity);
    }

    @Override
    public void saveOrUpdate(T entity) {
        getSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }

    @Override
    public void deleteAll() {
        List<T> entities = findAll();
        for (T entity : entities) {
            getSession().delete(entity);
        }
    }

    @Override
    public List<T> findAll() {
        return getSession().createCriteria(this.entityClass).list();
    }

}
