package com.gmail.eapbox.repository.model;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public enum OrderStatus {
    BASKET,
    NEW,
    REVIWING,
    IN_PROGRESS,
    DELIVERED
}
