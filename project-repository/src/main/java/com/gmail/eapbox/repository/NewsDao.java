package com.gmail.eapbox.repository;


import com.gmail.eapbox.repository.model.News;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface NewsDao extends GenericDao<News, Integer> {

}
