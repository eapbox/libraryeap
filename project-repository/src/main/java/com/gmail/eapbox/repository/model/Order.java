package com.gmail.eapbox.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Entity
@Table(name = "T_ORDER")
public class Order implements Serializable{
    private static final long serialVersionUID = -8327754981670932099L;

    @Id
    @Column(name = "F_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "F_AMOUNT")
    private Integer amount;

    @Column(name = "F_STATUS")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(name = "F_ORDER_NUM")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer orderNumber;

    @Column(name = "F_DATE")
    private String date;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "F_GOOD_ID", nullable = false)
    private Good good;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
