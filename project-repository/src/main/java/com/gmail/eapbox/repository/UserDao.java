package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.repository.model.UserRole;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface UserDao extends GenericDao<User,Integer> {

    User getUserByEmail(String username);

    void changeBlocked(Integer id, Boolean blocked);

    void changeRole(Integer id, UserRole role);
}
