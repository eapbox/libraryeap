package com.gmail.eapbox.repository.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Entity
@Table(name = "T_FILE")
public class FileEntity implements Serializable{
    private static final long serialVersionUID = -3605023383549138908L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Column(name = "F_NEWS_ID")
    private Integer newsId;

    @Column(name = "F_FILE_NAME")
    private String filename;

    @Column(name = "F_LOCATION")
    private String location;

    @OneToOne
    @PrimaryKeyJoinColumn
    private News news;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String fileName) {
        this.filename = fileName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileEntity that = (FileEntity) o;
        return Objects.equals(newsId, that.newsId) &&
            Objects.equals(filename, that.filename) &&
            Objects.equals(location, that.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newsId, filename, location);
    }
}
