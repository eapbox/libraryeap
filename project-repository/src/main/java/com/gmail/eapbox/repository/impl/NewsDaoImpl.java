package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.NewsDao;
import com.gmail.eapbox.repository.model.News;
import org.springframework.stereotype.Repository;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Repository
public class NewsDaoImpl
    extends GenericDaoImpl<News, Integer>
    implements NewsDao {

}
