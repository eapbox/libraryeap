package com.gmail.eapbox.repository.impl;

import com.gmail.eapbox.repository.OrderDao;
import com.gmail.eapbox.repository.model.Good;
import com.gmail.eapbox.repository.model.Order;
import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.repository.model.User;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Properties;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Repository
public class OrderDaoImpl
    extends GenericDaoImpl<Order, Integer>
    implements OrderDao {
    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class);

    @Autowired
    private Properties properties;

    @Override
    public List<Order> getBasket(String username) {
        logger.info("getBasket of " + username);

        String hql = properties.getProperty("select.all.from.order.where.orderstatus.username");
        Query query = getSession().createQuery(hql);
        query.setParameter("orderStatus", OrderStatus.BASKET);
        query.setParameter("username", username);

        return query.list();
    }

    @Override
    public Order getOrderBasketByGood(Good good) {
        logger.info("getGood from Basket: " + good.getInvNumber());

        String hql = properties.getProperty("select.all.from.order.where.goodId.orderStatus");
        Query query = getSession().createQuery(hql);
        query.setParameter("orderStatus", OrderStatus.BASKET);
        query.setParameter("goodId", good.getId());
        Object uniqueResult = query.uniqueResult();
        return uniqueResult != null ? (Order) uniqueResult : null;
    }

    @Override
    public List<Order> getUserOrders(String username) {
        logger.info("get orders of user: " + username);

        String hql = properties.getProperty("select.all.from.order.where.username.notOrder");
        Query query = getSession().createQuery(hql);
        query.setParameter("username", username);
        query.setParameter("orderStatus", OrderStatus.BASKET);

        return query.list();
    }

    @Override
    public List<Order> getAllOrders() {
        logger.info("getAllOrders");

        String hql = properties.getProperty("select.all.from.order.where.notOrder");
        Query query = getSession().createQuery(hql);
        query.setParameter("orderStatus", OrderStatus.BASKET);

        return query.list();
    }

    @Override
    public void setAmountGood(Integer id, Integer amount) {
        logger.info("setAmountGood: " + id + "; amount=" + amount);

        String hql = properties.getProperty("update.order.set.amount.where.id");
        Query query = getSession().createQuery(hql);
        query.setParameter(0, amount);
        query.setParameter(1, id);
        int res = query.executeUpdate();
    }

    @Override
    public void setStatus(OrderStatus status, User user) {
        logger.info("setStatus: " + status + "; user: " + user.getEmail());

        String hql = properties.getProperty("update.order.set.orderStatus.where.user");
        Query query = getSession().createQuery(hql);
        query.setParameter(0, status);
        query.setParameter(1, user);
        int res = query.executeUpdate();
    }

    @Override
    public void changeOrderStatus(Integer orderNumber, OrderStatus orderStatus) {

        String hql = properties.getProperty("update.order.set.orderStatus.where.orderNumber");
        Query query = getSession().createQuery(hql);
        query.setParameter(0, orderStatus);
        query.setParameter(1, orderNumber);
        int res = query.executeUpdate();
    }

    @Override
    public void deleteById(Integer id) {
        logger.info("deleteById: " + id);

        String hql = properties.getProperty("delete.order.where.id");
        Query query = getSession().createQuery(hql);
        query.setParameter(0, id);
        int res = query.executeUpdate();
    }

    @Override
    public Integer getCurrentOrderNumber(String username) {
        logger.info("getCurrentOrderNumber");

        String hql = properties.getProperty("select.orderNumber.from.order.where.username.orderStatus");
        Query query = getSession().createQuery(hql);
        query.setParameter("orderStatus", OrderStatus.BASKET);
        query.setParameter("email", username);
        Integer orderNumber = (Integer) query.uniqueResult();
        if (orderNumber != null)
            return orderNumber;

        hql = properties.getProperty("select.max.orderNumber.from.order");
        query = getSession().createQuery(hql);
        orderNumber = (Integer) query.uniqueResult();
        return orderNumber != null ? ++orderNumber : 1;
    }
}
