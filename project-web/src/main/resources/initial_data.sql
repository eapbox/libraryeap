insert into T_USER(F_EMAIL,F_PASSWORD,F_ROLE,F_BLOCKED) values ('user@mail.ru','$2a$04$R8o.TJpWQKnroodfZ6/aWeO6jUIQ3oPqKzDNgxBV1YbPPrLOiva.i','ROLE_USER',FALSE);
insert into T_USER_INFORMATION(F_NAME,F_SURNAME,F_FATHERNAME,F_PHONE_NUMBER,F_ADDRESS, F_INFO) values ('Ivan','Ivanov','Ivanovich','+375291111','brovki1','dop info 1');

insert into T_USER(F_EMAIL,F_PASSWORD,F_ROLE,F_BLOCKED) values ('user2@mail.ru','$2a$04$R8o.TJpWQKnroodfZ6/aWeO6jUIQ3oPqKzDNgxBV1YbPPrLOiva.i','ROLE_USER',FALSE);
insert into T_USER_INFORMATION(F_NAME,F_SURNAME,F_FATHERNAME,F_PHONE_NUMBER,F_ADDRESS, F_INFO) values ('Ivan2','Ivanov2','Ivanovich2','+375291111','brovki1','dop info 1');

insert into T_USER(F_EMAIL,F_PASSWORD,F_ROLE,F_BLOCKED) values ('admin@mail.ru','$2a$04$Fo8NGU8kJWcVGdHXOdsymONgxQuor6tw1z/xQcUwuifBcCuaULTSm','ROLE_ADMIN',FALSE);
insert into T_USER_INFORMATION(F_NAME,F_SURNAME,F_FATHERNAME,F_PHONE_NUMBER,F_ADDRESS, F_INFO) values ('Petr','Petrov','Petrovich','+375292222','brovki2','dop info 2');

insert into T_USER(F_EMAIL,F_PASSWORD,F_ROLE,F_BLOCKED) values ('root@mail.ru','$2a$04$Mo4GTKQX2YA2PxVINTTcNO74PEMtwz2.8d5xmECY./.JKr3TVKpvC','ROLE_ROOT',FALSE);
insert into T_USER_INFORMATION(F_NAME,F_SURNAME,F_FATHERNAME,F_PHONE_NUMBER,F_ADDRESS, F_INFO) values ('Sidor','Sidorov','Sidorovich','+375293333','brovki3','dop info 3');

insert into T_GOOD(F_INFO, F_INVNUMBER, F_BLOCKED, F_NAME, F_PRICE) VALUES ('info1',1111,FALSE ,'good1',1.25);
insert into T_GOOD(F_INFO, F_INVNUMBER, F_BLOCKED, F_NAME, F_PRICE) VALUES ('info2',2222,FALSE ,'good2',2.25);
insert into T_GOOD(F_INFO, F_INVNUMBER, F_BLOCKED, F_NAME, F_PRICE) VALUES ('info3',3333,FALSE ,'good3',3.25);
insert into T_GOOD(F_INFO, F_INVNUMBER, F_BLOCKED, F_NAME, F_PRICE) VALUES ('info4',4444,FALSE ,'good4',4.25);
insert into T_GOOD(F_INFO, F_INVNUMBER, F_BLOCKED, F_NAME, F_PRICE) VALUES ('info5',5555,FALSE ,'good5',5.25);