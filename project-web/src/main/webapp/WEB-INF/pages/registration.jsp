<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Registration new user</title>

    <!-- Bootstrap-->
    <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="p-x-1 p-y-3">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"><h2><spring:message code="registration"/></h2></div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form class="card card-block m-x-auto bg-faded form-width" method="post" modelAttribute="user"
                       action="/registration">
                <div class="form-group">
                    <label for="email"><spring:message code="registration.email"/></label>
                    <p class="bg-danger"><form:errors path="email"/></p>
                    <form:input id="email" cssClass="form-control" path="email" type="text"/>
                </div>
                <div class="form-group">
                    <label for="password"><spring:message code="registration.password"/></label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" path="password" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="confirmPassword"><spring:message code="registration.confirmPassword"/></label>
                    <p class="bg-danger"><form:errors path="confirmPassword"/></p>
                    <form:input id="confirmPassword" path="confirmPassword" cssClass="form-control" type="password"/>
                </div>

                <div class="form-group">
                    <label for="surname"><spring:message code="registration.surname"/></label>
                    <p class="bg-danger"><form:errors path="surname"/></p>
                    <form:input id="surname" path="surname" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="name"><spring:message code="registration.name"/></label>
                    <p class="bg-danger"><form:errors path="name"/></p>
                    <form:input id="name" path="name" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="fathername"><spring:message code="registration.fathername"/></label>
                    <p class="bg-danger"><form:errors path="fathername"/></p>
                    <form:input id="fathername" path="fathername" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="phone"><spring:message code="registration.phone"/></label>
                    <p class="bg-danger"><form:errors path="phone"/></p>
                    <form:input id="phone" path="phone" cssClass="form-control" type="tel"/>
                </div>

                <div class="form-group">
                    <label for="address"><spring:message code="registration.address"/></label>
                    <p class="bg-danger"><form:errors path="address"/></p>
                    <form:textarea rows="3" class="form-control" id="address" path="address"></form:textarea>
                </div>
                <div class="form-group">
                    <label for="addInfo"><spring:message code="registration.addInfo"/></label>
                    <p class="bg-danger"><form:errors path="addInfo"/></p>
                    <form:textarea rows="3" class="form-control" id="addInfo" path="addInfo"></form:textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-default"><spring:message code="save"/></button>
                    <input type="reset" class="btn btn-default" value="<spring:message code="registration.clear"/>">
                </div>
            </form:form>
            <form action="${pageContext.request.contextPath}/logout" method="get">
                <button type="submit" class="btn btn-default"><spring:message code="navbar.logout"/></button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>

</body>
</html>

