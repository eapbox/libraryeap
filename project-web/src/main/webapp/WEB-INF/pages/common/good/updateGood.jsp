<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<h1><spring:message code="good.updateGood"/>: <c:out value="${good.invNumber}"/></h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-8">
                    <form:form method="post" modelAttribute="good" enctype="multipart/form-data"
                               action="${pageContext.request.contextPath}/goods/update">
                        <div class="form-group">
                            <label for="name"><spring:message code="good.table.name"/></label>
                            <p class="bg-danger"><form:errors path="blocked"/></p>
                            <p class="bg-danger"><form:errors path="name"/></p>
                            <form:input id="name" cssClass="form-control" path="name" type="text" value="${good.name}"/>
                        </div>
                        <div class="form-group">
                            <label for="info"><spring:message code="good.table.info"/></label>
                            <p class="bg-danger"><form:errors path="info"/></p>
                            <form:textarea rows="3" class="form-control" id="info" path="info" value="${good.info}"/>
                        </div>
                        <div class="form-group">
                            <label for="price"><spring:message code="good.table.price"/></label>
                            <p class="bg-danger"><form:errors path="price"/></p>
                            <form:input id="price" cssClass="form-control" path="price" type="number" step="0.01" value="${good.price}"/>
                        </div>

                        <form:input type="hidden" path="id"/>
                        <form:input type="hidden" path="invNumber"/>
                        <button class="btn btn-default"><spring:message code="save"/></button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
