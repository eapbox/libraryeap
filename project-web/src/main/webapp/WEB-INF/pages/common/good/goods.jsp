<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<h1><spring:message code="good.nameTable"/></h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    <th>#</th>
                    <th><spring:message code="good.table.invNumber"/></th>
                    <th><spring:message code="good.table.name"/></th>
                    <th><spring:message code="good.table.info"/></th>
                    <th><spring:message code="good.table.price"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="good" items="${goodList}">
                    <tr>
                        <security:authorize access="hasAnyRole({'ROOT','ADMIN'})">
                            <td><input type="radio" name="selectedId" value="${good.id}"></td>
                        </security:authorize>
                        <security:authorize access="hasAnyRole({'USER'})">
                            <td>
                                <form action="${pageContext.request.contextPath}/goods/addBasket" method="post">
                                    <input type="hidden" name="id" value=<c:out value="${good.id}"/>>
                                    <input type="submit" width="60" value="+1">
                                </form>
                            </td>
                        </security:authorize>

                        <td><c:out value="${good.id}"/></td>
                        <td><c:out value="${good.invNumber}"/></td>
                        <td><c:out value="${good.name}"/></td>
                        <td><c:out value="${good.info}"/></td>
                        <td><c:out value="${good.price}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>