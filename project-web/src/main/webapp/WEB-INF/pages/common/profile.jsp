<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<form:form class="card card-block m-x-auto bg-faded form-width"
           action="${pageContext.request.contextPath}/users/profile/change"
           modelAttribute="currentUser" method="POST">
    <div class="form-group">
        <label class="control-label col-xs-3" for="surname"><spring:message code="registration.surname"/></label>
        <p class="bg-danger"><form:errors path="surname"/></p>
        <form:input id="surname" path="surname" cssClass="form-control" type="text"/>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="name"><spring:message code="registration.name"/></label>
        <p class="bg-danger"><form:errors path="name"/></p>
        <form:input type="text" cssClass="form-control" id="name" path="name"/>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="fathername"><spring:message code="registration.fathername"/></label>
        <p class="bg-danger"><form:errors path="fathername"/></p>
        <form:input type="text" cssClass="form-control" id="fathername" path="fathername"/>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="phone"><spring:message code="registration.phone"/></label>
        <p class="bg-danger"><form:errors path="phone"/></p>
        <form:input type="tel" cssClass="form-control" id="phone" path="phone"/>
    </div>

    <div class="form-group">
        <label class="control-label col-xs-3" for="address"><spring:message code="registration.address"/></label>
        <p class="bg-danger"><form:errors path="address"/></p>
        <form:textarea rows="3" cssClass="form-control" id="address" path="address"/>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="addInfo"><spring:message code="registration.addInfo"/></label>
        <p class="bg-danger"><form:errors path="addInfo"/></p>
        <form:textarea rows="3" class="form-control" id="addInfo" path="addInfo"/>
    </div>

    <br/>
    <div class="form-group">
        <label class="control-label col-xs-3" for="password"><spring:message code="registration.new.password"/></label>
        <p class="bg-danger"><form:errors path="password"/></p>
        <input type="password" class="form-control" id="password" name="password"
               placeholder="<spring:message code="registration.new.password.placeholder"/>">
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="confirmPassword"><spring:message
                code="registration.new.confirmPassword"/></label>
        <p class="bg-danger"><form:errors path="confirmPassword"/></p>
        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">
    </div>

    <div class="form-group">
        <div class="col-xs-offset-3 col-xs-9">
            <input type="submit" class="btn btn-primary" value="<spring:message code="registration.save"/>">
            <input type="reset" class="btn btn-default" value="<spring:message code="registration.cancel"/>">
        </div>
    </div>
</form:form>
