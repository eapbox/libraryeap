<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="title.main"/></title>

    <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>

    </style>
</head>
<body>
<%@include file="navbar.jsp" %>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <%@include file="leftMenu.jsp" %>
        </div>

        <div class="col-sm-8 text-left">
            <p>Welcome: <security:authentication property="principal.username"/></p>
            <p>You are <security:authentication property="principal.role"/></p>
            <br>

            <c:if test="${selectedNavbar == 'NEWS'}">
                <c:if test="${actionView == 'VIEW'}">
                    <%@include file="../news/news.jsp" %>
                </c:if>
                <c:if test="${actionView == 'ADD'}">
                    <%@include file="../news/addNews.jsp" %>
                </c:if>
                <c:if test="${actionView == 'UPDATE'}">
                    <%@include file="../news/updateNews.jsp" %>
                </c:if>
            </c:if>

            <c:if test="${selectedNavbar == 'USERS'}">
                <c:if test="${actionView == 'VIEW'}">
                    <%@include file="../users.jsp" %>
                </c:if>
            </c:if>

            <c:if test="${selectedNavbar == 'GOODS'}">
                <c:if test="${actionView == 'VIEW'}">
                    <%@include file="../good/goods.jsp" %>
                </c:if>
                <c:if test="${actionView == 'ADD'}">
                    <%@include file="../good/addGood.jsp" %>
                </c:if>
                <c:if test="${actionView == 'UPDATE'}">
                    <%@include file="../good/updateGood.jsp" %>
                </c:if>
                <c:if test="${actionView == 'COPY'}">
                    <%@include file="../good/copyGood.jsp" %>
                </c:if>
            </c:if>

            <c:if test="${selectedNavbar == 'BASKET'}">
                <c:if test="${actionView == 'VIEW'}">
                    <%@include file="../order/basket.jsp" %>
                </c:if>
            </c:if>

            <c:if test="${selectedNavbar == 'ORDERS'}">
                <c:if test="${actionView == 'VIEW'}">
                    <%@include file="../order/orders.jsp" %>
                </c:if>
            </c:if>

            <c:if test="${selectedNavbar == 'PROFILE'}">
                <c:if test="${actionView == 'VIEW'}">
                    <%@include file="../profile.jsp" %>
                </c:if>
            </c:if>

        </div>
    </div>
</div>

<%@include file="footer.jsp" %>

</body>
</html>
