<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<script>
    function radioAction(action){
        var nameRadio = document.getElementsByName('selectedId');
        rezultatRadio = 0;
        for (var i = 0; i < nameRadio.length; i++) {
            if (nameRadio[i].type === 'radio' && nameRadio[i].checked) {
                rezultatRadio = nameRadio[i].value;
            }
        }
        window.location.href = "${pageContext.request.contextPath}" + action + rezultatRadio;
    }
</script>

<c:if test="${selectedNavbar == 'NEWS'}">
    <h2><spring:message code="navbar.news"/></h2>
    <security:authorize access="hasAnyRole({'ROOT','ADMIN'})">
        <p><a href="${pageContext.request.contextPath}/news/add"><spring:message code="leftMenu.add"/></a></p>
        <p><a onclick="radioAction('/news/update?id=')"><spring:message code="leftMenu.update"/></a></p>
        <p><a onclick="radioAction('/news/delete?id=')"><spring:message code="leftMenu.del"/></a></p>
    </security:authorize>
</c:if>

<c:if test="${selectedNavbar == 'USERS'}">
    <h2><spring:message code="navbar.users"/></h2>
    <security:authorize access="hasAnyRole({'ROOT'})">
        <p><a onclick="radioAction('/users/delete?id=')"><spring:message code="leftMenu.del"/></a></p>
    </security:authorize>
</c:if>

<c:if test="${selectedNavbar == 'GOODS'}">
    <security:authorize access="hasAnyRole({'ROOT','ADMIN'})">
        <h2><spring:message code="navbar.goods"/></h2>
        <p><a href="${pageContext.request.contextPath}/goods/add"><spring:message code="leftMenu.add"/></a></p>
        <p><a onclick="radioAction('/goods/copy?id=')"><spring:message code="leftMenu.copy"/></a></p>
        <p><a onclick="radioAction('/goods/update?id=')"><spring:message code="leftMenu.update"/></a></p>
    </security:authorize>
    <security:authorize access="hasAnyRole({'USER'})">
        <h2><spring:message code="navbar.basket"/></h2>
        <br>
        <h4>position: <p><c:out value="${basketInfo.basketPosition}"/></p></h4>
        <h4>basket sum: <p><c:out value="${basketInfo.basketSum}"/></p></h4>
    </security:authorize>
</c:if>

<c:if test="${selectedNavbar == 'BASKET'}">
    <h2><spring:message code="navbar.basket"/></h2>
    <security:authorize access="hasAnyRole({'USER'})">
        <br>
        <h4>position: <p><c:out value="${basketInfo.basketPosition}"/></p></h4>
        <h4>basket sum: <p><c:out value="${basketInfo.basketSum}"/></p></h4>
        <br>
        <h2>
            <form action="${pageContext.request.contextPath}/basket/toOrder" method="post">
                <button type="submit"><spring:message code="leftMenu.toOrder"/></button>
            </form>
        </h2>
    </security:authorize>
</c:if>

<c:if test="${selectedNavbar == 'PROFILE'}">
    <h2><spring:message code="navbar.profile"/></h2>
    <h4><security:authentication property="principal.username"/></h4>
</c:if>

<c:if test="${selectedNavbar == 'ORDERS'}">
    <h2><spring:message code="navbar.orders"/></h2>

</c:if>