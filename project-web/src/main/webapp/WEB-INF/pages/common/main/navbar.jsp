<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><spring:message code="navbar.logo"/></a>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">

                <security:authorize access="hasAnyRole({'ROOT','ADMIN','USER'})">
                    <c:if test="${selectedNavbar == 'GOODS'}">
                        <li class="active"><a href="/goods"><spring:message code="navbar.goods"/></a></li>
                    </c:if>
                    <c:if test="${selectedNavbar != 'GOODS'}">
                        <li><a href="/goods"><spring:message code="navbar.goods"/></a></li>
                    </c:if>
                </security:authorize>

                <security:authorize access="hasAnyRole({'USER'})">
                    <c:if test="${selectedNavbar == 'BASKET'}">
                        <li class="active"><a href="/basket"><spring:message code="navbar.basket"/></a></li>
                    </c:if>
                    <c:if test="${selectedNavbar != 'BASKET'}">
                        <li><a href="/basket"><spring:message code="navbar.basket"/></a></li>
                    </c:if>
                </security:authorize>

                <security:authorize access="hasAnyRole({'ROOT','ADMIN','USER'})">
                    <c:if test="${selectedNavbar == 'ORDERS'}">
                        <li class="active"><a href="/orders"><spring:message code="navbar.orders"/></a></li>
                    </c:if>
                    <c:if test="${selectedNavbar != 'ORDERS'}">
                        <li><a href="/orders"><spring:message code="navbar.orders"/></a></li>
                    </c:if>
                </security:authorize>

                <security:authorize access="hasAnyRole({'ROOT'})">
                    <c:if test="${selectedNavbar == 'USERS'}">
                        <li class="active"><a href="/users"><spring:message code="navbar.users"/></a></li>
                    </c:if>
                    <c:if test="${selectedNavbar != 'USERS'}">
                        <li><a href="/users"><spring:message code="navbar.users"/></a></li>
                    </c:if>
                </security:authorize>

                <security:authorize access="hasAnyRole({'USER'})">
                    <c:if test="${selectedNavbar == 'PROFILE'}">
                        <li class="active"><a href="users/profile/change"><spring:message code="navbar.profile"/></a></li>
                    </c:if>
                    <c:if test="${selectedNavbar != 'PROFILE'}">
                        <li><a href="users/profile/change"><spring:message code="navbar.profile"/></a></li>
                    </c:if>
                </security:authorize>

                <security:authorize access="hasAnyRole({'ROOT','ADMIN','USER'})">
                    <c:if test="${selectedNavbar == 'NEWS'}">
                        <li class="active"><a href="/news"><spring:message code="navbar.news"/></a></li>
                    </c:if>
                    <c:if test="${selectedNavbar != 'NEWS'}">
                        <li><a href="/news"><spring:message code="navbar.news"/></a></li>
                    </c:if>
                </security:authorize>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span>
                    <spring:message code="navbar.logout"/>
                </a></li>
            </ul>
        </div>
    </div>
</nav>