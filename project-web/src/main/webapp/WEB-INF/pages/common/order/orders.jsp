<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<h1><spring:message code="navbar.orders"/></h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <%--<th></th>
                    <th>#</th>--%>
                    <th><spring:message code="order.table.numOrder"/></th>
                    <th><spring:message code="order.table.username"/></th>
                    <th><spring:message code="order.table.amount"/></th>
                    <th><spring:message code="order.table.sum"/></th>
                    <th><spring:message code="order.table.status"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="order" items="${orders}">
                    <tr>
                            <%--<td><input type="checkbox" name="selectedId" value="${order.numOrder}"></td>--%>
                        <td><c:out value="${order.orderNumber}"/></td>
                        <td><c:out value="${order.username}"/></td>
                        <td><c:out value="${order.goodAmount}"/></td>
                        <td><c:out value="${order.orderSum}"/></td>
                        <td>
                            <security:authorize access="hasAnyRole({'USER'})">
                                <c:out value="${order.orderStatus}"/>
                            </security:authorize>
                            <security:authorize access="hasAnyRole({'ROOT','ADMIN'})">
                                <form action="${pageContext.request.contextPath}/orders/changeStatus" method="POST">
                                    <input type="hidden" name="orderNumber" value="${order.orderNumber}">
                                    <select required name="orderStatus">
                                        <option><c:out value="${order.orderStatus}"/></option>
                                        <c:forEach var="orderStatus" items="${orderStatuses}">
                                            <c:if test="${orderStatus != 'BASKET'}">
                                                <c:if test="${orderStatus != order.orderStatus}">
                                                    <option value="${orderStatus}">
                                                        <c:out value="${orderStatus}"/>
                                                    </option>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                    <input type="submit" value="Change">
                                </form>
                            </security:authorize>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>