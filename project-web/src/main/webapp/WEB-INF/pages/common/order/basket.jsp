<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<h1><spring:message code="basket.nameTable"/></h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th><spring:message code="good.table.invNumber"/></th>
                    <th><spring:message code="good.table.name"/></th>
                    <th><spring:message code="good.table.info"/></th>
                    <th><spring:message code="good.table.price"/></th>
                    <th><spring:message code="good.table.amount"/></th>
                    <th><spring:message code="good.table.Sum"/></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="basket" items="${basketList}">
                    <tr>
                        <td><c:out value="${basket.id}"/></td>
                        <td><c:out value="${basket.goodInvNumber}"/></td>
                        <td><c:out value="${basket.goodName}"/></td>
                        <td><c:out value="${basket.goodInfo}"/></td>
                        <td><c:out value="${basket.goodPrice}"/></td>
                        <td><c:out value="${basket.goodAmount}"/></td>
                        <td><c:out value="${basket.totalSum}"/></td>
                        <td>
                            <form action="${pageContext.request.contextPath}/basket/addOne" method="post">
                                <input type="hidden" name="id" value=<c:out value="${basket.id}"/>>
                                <input type="hidden" name="amount" value=<c:out value="${basket.goodAmount}"/>>
                                <input type="submit" width="60" value="+1">
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/basket/subOne" method="post">
                                <input type="hidden" name="id" value=<c:out value="${basket.id}"/>>
                                <input type="hidden" name="amount" value=<c:out value="${basket.goodAmount}"/>>
                                <input type="submit" width="60" value="-1">
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/basket/delete" method="post">
                                <input type="hidden" name="id" value=<c:out value="${basket.id}"/>>
                                <input type="submit" width="60" value="DELETE">
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
