<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    <th>#</th>
                    <th><spring:message code="registration.email"/></th>
                    <th><spring:message code="registration.role"/></th>
                    <th><spring:message code="registration.blocked"/></th>
                    <th><spring:message code="registration.name"/></th>
                    <th><spring:message code="registration.surname"/></th>
                    <th><spring:message code="registration.fathername"/></th>
                    <th><spring:message code="registration.address"/></th>
                    <th><spring:message code="registration.phone"/></th>
                    <th><spring:message code="registration.addInfo"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="user" items="${usersList}">
                    <tr>
                        <td><input type="radio" name="selectedId" value="${user.id}"></td>
                        <td><c:out value="${user.id}"/></td>
                        <td><c:out value="${user.email}"/></td>
                        <td>
                            <form action="${pageContext.request.contextPath}/users/changeRole" method="POST">
                                <input type="hidden" name="id" value="${user.id}">
                                <select required name="role">
                                    <option><c:out value="${user.role}"/></option>
                                    <c:forEach var="userRole" items="${userRoles}">
                                        <c:if test="${userRole != user.role}">
                                            <option value="${userRole}">
                                                <c:out value="${userRole}"/>
                                            </option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                                <input type="submit" value="Change">
                            </form>
                        </td>

                        <td>
                            <form action="${pageContext.request.contextPath}/users/changeBlocked" method="POST">
                                <c:choose>
                                    <c:when test="${user.blocked}">
                                        <input type="checkbox" checked>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="checkbox" name="asd">
                                    </c:otherwise>
                                </c:choose>
                                <input type="hidden" name="id" value=<c:out value="${user.id}"/>>
                                <input type="hidden" name="blocked" value=<c:out value="${!user.blocked}"/>>
                                <input type="submit" value="Change">
                            </form>
                        </td>

                        <td><c:out value="${user.name}"/></td>
                        <td><c:out value="${user.surname}"/></td>
                        <td><c:out value="${user.fathername}"/></td>
                        <td><c:out value="${user.address}"/></td>
                        <td><c:out value="${user.phone}"/></td>
                        <td><c:out value="${user.addInfo}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
