<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<h1><spring:message code="news.addNews"/></h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                 <div class="col-md-8">
                    <form:form method="post" modelAttribute="news" enctype="multipart/form-data"
                               action="${pageContext.request.contextPath}/news/add">
                        <div class="form-group">
                            <label for="caption"><spring:message code="news.add.caption"/></label>
                            <p class="bg-danger"><form:errors path="caption"/></p>
                            <form:input id="caption" cssClass="form-control" path="caption" type="text"/>
                        </div>
                        <div class="form-group">
                            <label for="content"><spring:message code="news.add.content"/></label>
                            <p class="bg-danger"><form:errors path="content"/></p>
                            <form:textarea rows="3" class="form-control" id="content" path="content"></form:textarea>
                        </div>
                        <div class="form-group">
                            <label for="file">File</label>
                            <form:input id="file" path="file" cssClass="form-contro${news.id}l" type="file"/>
                        </div>
                        <button class="btn btn-default"><spring:message code="save"/></button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>