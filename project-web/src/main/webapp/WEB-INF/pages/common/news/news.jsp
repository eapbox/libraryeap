<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<h1><spring:message code="news.nameTable"/></h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    <th>#</th>
                    <th><spring:message code="news.table.caption"/></th>
                    <th><spring:message code="news.table.content"/></th>
                    <th><spring:message code="news.table.image"/></th>
                    <th><spring:message code="news.table.username"/></th>
                    <th><spring:message code="news.table.data"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="news" items="${newsList}">
                    <tr>
                        <td><input type="radio" name="selectedId" value="${news.id}"></td>
                        <td><c:out value="${news.id}"/></td>
                        <td><c:out value="${news.caption}"/></td>
                        <td><c:out value="${news.content}"/></td>
                        <td>
                            <img src="${pageContext.request.contextPath}/news/download/<c:out value="${news.id}"/>" alt="${news.filename}"
                                 height="80" width="80">
                        </td>
                        <td><c:out value="${news.username}"/></td>
                        <td><c:out value="${news.date}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
