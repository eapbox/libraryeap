<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ERROR</title>

    <!-- Bootstrap-->
    <link href="${pageContext.request.contextPath}/resources/css/errorPage.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="error-template">
            <h1>Oops!</h1>
            <div class="error-details">
                Sorry, an error has occured, Requested page not found!<br>
            </div>
            <div class="error-actions">
                <a href="/login" class="btn btn-primary">
                    <i class="icon-home icon-white"></i> Take Me Home </a>

            </div>
        </div>
    </div>
</div>

</body>
</html>
