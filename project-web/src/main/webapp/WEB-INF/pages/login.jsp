<%--
  Created by IntelliJ IDEA.
  User: Ershov Aliaksandr
  Date: .08.2017
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="title.login"/></title>

    <!-- Bootstrap-->
    <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="p-x-1 p-y-3">
    <form class="card card-block m-x-auto bg-faded form-width" action="login" method="POST">
        <legend class="m-b-1 text-xs-center"><spring:message code="authorization"/></legend>
        <c:if test="${param['error']}">
            <p class="bg-danger"><spring:message code="authorization.invalidUserPassword"/></p>
        </c:if>
        <div class="form-group input-group">
            <span class="input-group-addon">@</span>
            <span class="has-float-label">
                <label for="email"><spring:message code="authorization.email"/></label>
                <input class="form-control" id="email" type="email" name="username" placeholder="name@example.com"/>
            </span>
        </div>
        <div class="form-group has-float-label">
            <label for="password"><spring:message code="authorization.password"/></label>
            <input class="form-control" id="password" type="password" name="password" placeholder="••••••••"/>
        </div>
        <div class="text-xs-center">
            <button class="btn btn-block btn-primary" type="submit"><spring:message code="authorization.login"/></button>
        </div>
        <br>
        <p class="message"><spring:message code="authorization.notRegistered"/>
        <a href="registration"><spring:message code="registration.create"/></a>
    </form>
</div>
</body>
</html>
