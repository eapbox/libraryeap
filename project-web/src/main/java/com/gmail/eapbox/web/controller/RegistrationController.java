package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.UserService;
import com.gmail.eapbox.service.model.UserDTO;
import com.gmail.eapbox.web.controller.validator.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Properties;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class RegistrationController {
    private static final Logger logger = Logger.getLogger(RegistrationController.class);

    private final UserService userService;
    private final UserValidator userValidator;

    @Autowired
    public RegistrationController(
        UserService userService,
        UserValidator userValidator
    ) {
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationPage(Model model) {
        logger.info("Get - start");
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String saveNewUser(@ModelAttribute("user") UserDTO user,
                              BindingResult result) {
        logger.info("saveNewUser: " + user.getEmail());
        userValidator.validate(user, result);
        if (!result.hasErrors()) {
            userService.save(user);
            return "redirect:/login";
        } else {
            return "registration";
        }
    }
}
