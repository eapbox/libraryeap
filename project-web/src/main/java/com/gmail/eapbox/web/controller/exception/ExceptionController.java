package com.gmail.eapbox.web.controller.exception;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    private static final Logger logger = Logger.getLogger(ExceptionController.class);

    @ExceptionHandler(value = RuntimeException.class)
    public String processRuntimeException(RuntimeException e){
        logger.error(e.getMessage(), e);
        return "exception/error.jsp";
    }
}
