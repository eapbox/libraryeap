package com.gmail.eapbox.web.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class MainController {
    private static final Logger logger = Logger.getLogger(MainController.class);

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String showMainPage() {
        logger.info("Get - start");
        return "common/main/main";
    }
}
