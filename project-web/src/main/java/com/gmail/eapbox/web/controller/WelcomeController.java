package com.gmail.eapbox.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class WelcomeController {
    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String showWelcomePage() {
        return "login";
    }
}
