package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.service.NewsService;
import com.gmail.eapbox.service.model.NewsDTO;
import com.gmail.eapbox.web.controller.validator.NewsValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class NewsController {
    private static final Logger logger = Logger.getLogger(NewsController.class);

    private final NewsService newsService;
    private final NewsValidator newsValidator;

    public NewsController(NewsService newsService, NewsValidator newsValidator) {
        this.newsService = newsService;
        this.newsValidator = newsValidator;
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showAllNews(Model model
    ) {
        logger.info("showAllNews");

        List<NewsDTO> newsList = newsService.getAll();
        model.addAttribute("newsList", newsList);
        model.addAttribute("selectedNavbar", "NEWS");
        model.addAttribute("actionView", "VIEW");
        return "common/main/main";
    }

    @RequestMapping(value = "/news/add", method = RequestMethod.GET)
    public String showAddNewsPage(
        @ModelAttribute("news") NewsDTO news,
        Model model) {
        logger.info("showAddNewsPage");

        model.addAttribute("selectedNavbar", "NEWS");
        model.addAttribute("actionView", "ADD");
        return "common/main/main";
    }

    @RequestMapping(value = "/news/add", method = RequestMethod.POST)
    public String saveNews(
        @ModelAttribute("news") NewsDTO news,
        BindingResult result,
        Model model
    ) throws IOException {
        logger.info("saveNews: ");

        newsValidator.validate(news, result);
        if (!result.hasErrors()) {
            newsService.save(news);
            return "redirect:/news";
        } else {
            model.addAttribute("news", news);
            model.addAttribute("selectedNavbar", "NEWS");
            model.addAttribute("actionView", "ADD");
            return "common/main/main";
        }
    }

    @RequestMapping(value = "/news/update", method = RequestMethod.GET)
    public String showUpdateNewsPage(
        @RequestParam("id") Integer id,
        Model model
    ) throws IOException {
        logger.info("showUpdateNewsPage id: " + id);

        if (id > 0) {
            NewsDTO updateNews = newsService.getById(id);
            model.addAttribute("news", updateNews);
            model.addAttribute("selectedNavbar", "NEWS");
            model.addAttribute("actionView", "UPDATE");
            return "common/main/main";
        }
        return "redirect:/news";
    }

    @RequestMapping(value = "/news/update", method = RequestMethod.POST)
    public String updateNews(
        @ModelAttribute("news") NewsDTO news,
        BindingResult result,
        Model model
    ) throws IOException {
        logger.info("updateNews #: " + news.getId());

        newsValidator.validate(news, result);
        if (!result.hasErrors()) {
            newsService.update(news);
            return "redirect:/news";
        } else {
            model.addAttribute("news", news);
            model.addAttribute("selectedNavbar", "NEWS");
            model.addAttribute("actionView", "UPDATE");
            return "common/main/main";
        }
    }

    @RequestMapping(value = "news/delete", method = RequestMethod.GET)
    public String deleteSelectedNews(
        @RequestParam("id") Integer id
    ) throws IOException {
        logger.info("delete News id: " + id);

        newsService.delete(id);
        return "redirect:/news";
    }
}
