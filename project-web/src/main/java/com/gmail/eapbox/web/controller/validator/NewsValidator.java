package com.gmail.eapbox.web.controller.validator;

import com.gmail.eapbox.service.NewsService;
import com.gmail.eapbox.service.model.NewsDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Ershov Aliaksandr on 29.08.2017.
 */

@Component
public class NewsValidator implements Validator {
    private static final Logger logger = Logger.getLogger(NewsValidator.class);

    private final NewsService newsService;

    @Autowired
    public NewsValidator(NewsService newsService) {
        this.newsService = newsService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(NewsDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        NewsDTO news = (NewsDTO) o;
        logger.info("validate News: " + news.getCaption());

        ValidationUtils.rejectIfEmpty(errors, "caption", "error.news.caption.empty");
        ValidationUtils.rejectIfEmpty(errors, "content", "error.news.content.empty");
    }
}
