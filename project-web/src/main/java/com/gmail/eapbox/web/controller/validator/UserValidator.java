package com.gmail.eapbox.web.controller.validator;

import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.service.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ershov Aliaksandr on .08.2017.
 */

@Component
public class UserValidator implements Validator {

    private final UserDetailsService userDetailsService;

    @Autowired
    public UserValidator(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;
        String patternEmail = "^(.+)@(.+)$";

        ValidationUtils.rejectIfEmpty(errors, "email", "error.email.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "error.password.empty");
        ValidationUtils.rejectIfEmpty(errors, "confirmPassword", "error.confirmPassword.empty");
        ValidationUtils.rejectIfEmpty(errors, "name", "error.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "surname", "error.surname.empty");
        ValidationUtils.rejectIfEmpty(errors, "fathername", "error.fathername.empty");
        ValidationUtils.rejectIfEmpty(errors, "phone", "error.phone.empty");
        ValidationUtils.rejectIfEmpty(errors, "address", "error.address.empty");

        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
            errors.rejectValue("email", "error.email.exist");
        } catch (UsernameNotFoundException e){}

        Pattern p = Pattern.compile(patternEmail);
        Matcher m = p.matcher(user.getEmail());
        if (!m.matches()) {
            errors.rejectValue("email", "error.email.notEmail");
        }

        if (user.getPassword().length() < 3) {
            errors.rejectValue("password", "error.password.length");
        }

        if (!user.getPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("password", "error.password.notValid");
        }
    }

    public void validateNewProfile(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;
        String patternEmail = "^(.+)@(.+)$";

        ValidationUtils.rejectIfEmpty(errors, "name", "error.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "surname", "error.surname.empty");
        ValidationUtils.rejectIfEmpty(errors, "fathername", "error.fathername.empty");
        ValidationUtils.rejectIfEmpty(errors, "phone", "error.phone.empty");
        ValidationUtils.rejectIfEmpty(errors, "address", "error.address.empty");

        if (!user.getPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("password", "error.password.notValid");
        }

        if (user.getPassword().length()>0) {
            Pattern p = Pattern.compile(patternEmail);
            Matcher m = p.matcher(user.getEmail());
            if (!m.matches()) {
                errors.rejectValue("email", "error.email.notEmail");
            }
            if (user.getPassword().length() < 3) {
                errors.rejectValue("password", "error.password.length");
            }
        }
    }
}
