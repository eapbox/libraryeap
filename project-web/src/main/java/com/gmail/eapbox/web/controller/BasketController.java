package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.service.BasketService;
import com.gmail.eapbox.service.model.BasketInfoDTO;
import com.gmail.eapbox.service.model.BasketDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class BasketController {
    private static final Logger logger = Logger.getLogger(BasketController.class);

    private final BasketService basketService;

    @Autowired
    public BasketController(BasketService basketService) {
        this.basketService = basketService;
    }

    @RequestMapping(value = "/basket", method = RequestMethod.GET)
    public String showCurentBasket(Model model) {
        logger.info("showCurentBasket - start");

        BasketInfoDTO basketInfo = basketService.getBasketInfo();
        model.addAttribute("basketInfo", basketInfo);
        List<BasketDTO> basketList = basketService.getCurentBasket();
        model.addAttribute("basketList", basketList);
        model.addAttribute("selectedNavbar", "BASKET");
        model.addAttribute("actionView", "VIEW");
        return "common/main/main";
    }

    @RequestMapping(value = "/basket/addOne", method = RequestMethod.POST)
    public String addOneGood(
        @RequestParam("id") Integer id,
        @RequestParam("amount") Integer amount
    ) {
        logger.info("addOneGood id: " + id);

        basketService.setAmount(id, ++amount);
        return "redirect:/basket";
    }

    @RequestMapping(value = "/basket/subOne", method = RequestMethod.POST)
    public String subOneGood(
        @RequestParam("id") Integer id,
        @RequestParam("amount") Integer amount
    ) {
        logger.info("subOneGood id: " + id);

        basketService.setAmount(id, --amount);
        return "redirect:/basket";
    }

    @RequestMapping(value = "/basket/delete", method = RequestMethod.POST)
    public String deleteSelectedGood(
        @RequestParam("id") Integer id
    ) throws IOException {
        logger.info("delete Good id: " + id);

        basketService.setAmount(id, 0);
        return "redirect:/basket";
    }

    @RequestMapping(value = "/basket/toOrder", method = RequestMethod.POST)
    public String basketToOrder(Model model) {
        logger.info("basketToOrder");

        basketService.basketToOrder();
        return "redirect:/basket";
    }
}
