package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.GoodService;
import com.gmail.eapbox.service.BasketService;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import com.gmail.eapbox.service.model.BasketInfoDTO;
import com.gmail.eapbox.service.model.GoodDTO;
import com.gmail.eapbox.web.controller.validator.GoodValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class GoodController {
    private static final Logger logger = Logger.getLogger(GoodController.class);

    private final GoodService goodService;
    private final BasketService basketService;
    private final GoodValidator goodValidator;

    @Autowired
    public GoodController(GoodService goodService,
                          BasketService basketService,
                          GoodValidator goodValidator) {
        this.goodService = goodService;
        this.basketService = basketService;
        this.goodValidator = goodValidator;
    }

    @RequestMapping(value = "/goods", method = RequestMethod.GET)
    public String showAllGoods(Model model) {
        logger.info("Get - start");

        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
            .getAuthentication()
            .getPrincipal();

        if (principal.getRole() == UserRole.ROLE_USER) {
            BasketInfoDTO basketInfo = basketService.getBasketInfo();
            model.addAttribute("basketInfo", basketInfo);
        }

        List<GoodDTO> goodList = goodService.getAll();
        model.addAttribute("goodList", goodList);
        model.addAttribute("selectedNavbar", "GOODS");
        model.addAttribute("actionView", "VIEW");
        return "common/main/main";
    }

    @RequestMapping(value = "/goods/add", method = RequestMethod.GET)
    public String showAddGoodPage(
        @ModelAttribute("good") GoodDTO good,
        Model model
    ) {
        logger.info("Get - start");

        model.addAttribute("selectedNavbar", "GOODS");
        model.addAttribute("actionView", "ADD");
        return "common/main/main";
    }

    @RequestMapping(value = "/goods/add", method = RequestMethod.POST)
    public String saveNewGood(
        @ModelAttribute("good") GoodDTO good,
        BindingResult result,
        Model model
    ) throws IOException {
        logger.info("saveNewGood - start");

        goodValidator.validate(good, result);
        if (!result.hasErrors()) {
            goodService.save(good);
            return "redirect:/goods";
        } else {
            model.addAttribute("good", good);
            model.addAttribute("selectedNavbar", "GOODS");
            model.addAttribute("actionView", "ADD");
            return "common/main/main";
        }
    }

    @RequestMapping(value = "/goods/update", method = RequestMethod.GET)
    public String showUpdateGoodPage(
        @RequestParam("id") Integer id,
        Model model
    ) throws IOException {
        logger.info("showUpdateGoodPage id: " + id);

        if (id > 0) {
            GoodDTO updateGood = goodService.getById(id);
            model.addAttribute("good", updateGood);
            model.addAttribute("selectedNavbar", "GOODS");
            model.addAttribute("actionView", "UPDATE");
            return "common/main/main";
        }
        return "redirect:/goods";
    }

    @RequestMapping(value = "/goods/update", method = RequestMethod.POST)
    public String updateGood(
        @ModelAttribute("good") GoodDTO good,
        BindingResult result,
        Model model
    ) throws IOException {
        logger.info("updateGood #: " + good.getInvNumber());

        good.setBlocked(goodService.getByInvNumber(good.getInvNumber()).getBlocked());
        goodValidator.validateUpdate(good, result);
        if (!result.hasErrors()) {
            goodService.update(good);
            return "redirect:/goods";
        } else {
            model.addAttribute("good", good);
            model.addAttribute("selectedNavbar", "GOODS");
            model.addAttribute("actionView", "UPDATE");
            return "common/main/main";
        }
    }

    @RequestMapping(value = "/goods/copy", method = RequestMethod.GET)
    public String showCopyGoodPage(
        @RequestParam("id") Integer id,
        Model model
    ) throws IOException {
        logger.info("CopyGood: " + id);

        if (id > 0) {
            GoodDTO copyGood = goodService.getById(id);
            copyGood.setInvNumber(null);
            model.addAttribute("good", copyGood);
            model.addAttribute("selectedNavbar", "GOODS");
            model.addAttribute("actionView", "COPY");
            return "common/main/main";
        }
        return "redirect:/goods";
    }

    @RequestMapping(value = "/goods/copy", method = RequestMethod.POST)
    public String copyGood(
        @ModelAttribute("good") GoodDTO good,
        BindingResult result,
        Model model
    ) throws IOException {
        logger.info("copyGood: " + good.getName());

        goodValidator.validate(good, result);
        if (!result.hasErrors()) {
            goodService.save(good);
            return "redirect:/goods";
        } else {
            model.addAttribute("good", good);
            model.addAttribute("selectedNavbar", "GOODS");
            model.addAttribute("actionView", "COPY");
            return "common/main/main";
        }
    }

    @RequestMapping(value = "/goods/addBasket", method = RequestMethod.POST)
    public String addBasketSelectedOrder(
        @RequestParam("id") Integer id
    ) throws IOException {
        logger.info("addBasketSelectedGood: " + id);

        if (id > 0) {
            goodService.addToBasket(id);
        }
        return "redirect:/goods";
    }

}
