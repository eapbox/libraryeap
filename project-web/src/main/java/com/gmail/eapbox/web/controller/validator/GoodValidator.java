package com.gmail.eapbox.web.controller.validator;

import com.gmail.eapbox.service.GoodService;
import com.gmail.eapbox.service.model.GoodDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Ershov Aliaksandr on .08.2017.
 */

@Component
public class GoodValidator implements Validator {
    private static final Logger logger = Logger.getLogger(GoodValidator.class);

    private final GoodService goodService;

    @Autowired
    public GoodValidator(GoodService goodService) {
        this.goodService = goodService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(GoodDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        GoodDTO good = (GoodDTO) o;
        logger.info("validate Good: " + good.getInvNumber());

        ValidationUtils.rejectIfEmpty(errors, "invNumber", "error.good.invNumber.empty");
        ValidationUtils.rejectIfEmpty(errors, "name", "error.good.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "info", "error.good.info.empty");
        ValidationUtils.rejectIfEmpty(errors, "price", "error.good.price.empty");

        GoodDTO good2 = goodService.getByInvNumber(good.getInvNumber());
        if (good2 != null) {
            errors.rejectValue("invNumber", "error.good.invNumber.exist");
        }
    }

    public void validateUpdate(Object o, Errors errors) {
        GoodDTO good = (GoodDTO) o;
        logger.info("validate Good: " + good.getInvNumber());

        ValidationUtils.rejectIfEmpty(errors, "name", "error.good.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "info", "error.good.info.empty");
        ValidationUtils.rejectIfEmpty(errors, "price", "error.good.price.empty");

        if (good.getBlocked() != null) {
            if (good.getBlocked()) {
                errors.rejectValue("blocked", "error.good.invNumber.blocked");
            }
        }
    }
}
