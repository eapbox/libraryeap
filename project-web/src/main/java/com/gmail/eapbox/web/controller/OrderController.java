package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.service.OrderService;
import com.gmail.eapbox.service.model.OrderDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Ershov on xx.08.2017.
 */

@Controller
public class OrderController {
    private static final Logger logger = Logger.getLogger(OrderController.class);

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String showOrders(Model model) {
        logger.info("show orders");

        List<OrderDTO> orders = orderService.getUserOrders();
        OrderStatus orderStatuses[] = OrderStatus.values();
        model.addAttribute("orders", orders);
        model.addAttribute("orderStatuses", orderStatuses);
        model.addAttribute("selectedNavbar", "ORDERS");
        model.addAttribute("actionView", "VIEW");
        return "common/main/main";
    }

    @RequestMapping(value = "orders/changeStatus", method = RequestMethod.POST)
    public String changeOrderStatus(
        @RequestParam("orderNumber") Integer orderNumber,
        @RequestParam("orderStatus") OrderStatus orderStatus
    ) {
        logger.info("changeOrderStatus: " + orderNumber + "; new Status=" + orderStatus);

        orderService.changeOrderStatus(orderNumber, orderStatus);
        return "redirect:/orders";
    }

}
