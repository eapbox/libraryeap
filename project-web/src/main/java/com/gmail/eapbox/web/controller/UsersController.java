package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.UserService;
import com.gmail.eapbox.service.model.UserDTO;
import com.gmail.eapbox.web.controller.validator.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class UsersController {
    private static final Logger logger = Logger.getLogger(UsersController.class);

    private final UserService usersService;
    private final UserValidator userValidator;

    @Autowired
    public UsersController(UserService usersService,
                           UserValidator userValidator) {
        this.usersService = usersService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showAllUsers(Model model) {
        logger.info("Get - start");

        List<UserDTO> usersList = usersService.getAll();
        UserRole userRoles[] = UserRole.values();
        model.addAttribute("usersList", usersList);
        model.addAttribute("userRoles", userRoles);
        model.addAttribute("selectedNavbar", "USERS");
        model.addAttribute("actionView", "VIEW");
        return "common/main/main";
    }

    @RequestMapping(value = "users/delete", method = RequestMethod.GET)
    public String deleteSelectedUser(@RequestParam("id") Integer id) throws IOException {
        logger.info("deleteSelectedUser id: " + id);

        if (id > 0) {
            usersService.delete(id);
        }
        return "redirect:/users";
    }

    @RequestMapping(value = "users/changeRole", method = RequestMethod.POST)
    public String changeRoleUser(
        @RequestParam("id") Integer id,
        @RequestParam("role") UserRole role
    ) {
        logger.info("changeRoleUser id: " + id);

        usersService.changeRole(id, role);
        return "redirect:/users";
    }

    @RequestMapping(value = "users/changeBlocked", method = RequestMethod.POST)
    public String changeBlockedUser(
        @RequestParam("id") Integer id,
        @RequestParam("blocked") Boolean blocked
    ) {
        logger.info("changeBlocked id: " + id);

        usersService.changeBlocked(id, blocked);
        return "redirect:/users";
    }

    @RequestMapping(value = "users/profile/change", method = RequestMethod.GET)
    public String showCurentProfile(Model model) {
        logger.info("showCurentProfile");

        UserDTO user = usersService.getCurrentUser();
        model.addAttribute("currentUser", user);
        model.addAttribute("selectedNavbar", "PROFILE");
        model.addAttribute("actionView", "VIEW");
        return "common/main/main";
    }

    @RequestMapping(value = "users/profile/change", method = RequestMethod.POST)
    public String changeUserProfile(@ModelAttribute("currentUser") UserDTO updateUser,
                                    BindingResult result,
                                    Model model) {
        logger.info("saveUserProfile");

        userValidator.validateNewProfile(updateUser, result);
        if (!result.hasErrors()) {
            usersService.updateCurrentUser(updateUser);
            return "redirect:/logout";
        } else {
            model.addAttribute("currentUser", updateUser);
            model.addAttribute("selectedNavbar", "PROFILE");
            model.addAttribute("actionView", "VIEW");
            return "common/main/main";
        }
    }
}
