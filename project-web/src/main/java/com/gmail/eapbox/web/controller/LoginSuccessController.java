package com.gmail.eapbox.web.controller;

import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Controller
public class LoginSuccessController {

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl(Model model) {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        if (principal.getRole() == UserRole.ROLE_USER) {
            return "redirect:/news";
        } else if (principal.getRole() == UserRole.ROLE_ADMIN) {
            return "redirect:/news";
        } else if (principal.getRole() == UserRole.ROLE_ROOT) {
            return "redirect:/news";
        } else {
            return "redirect:/login";
        }
    }
}
