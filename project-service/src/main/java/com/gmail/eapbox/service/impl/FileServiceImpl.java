package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.FileDao;
import com.gmail.eapbox.repository.model.FileEntity;
import com.gmail.eapbox.service.FileService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

/**
 * Created by Ershov Aliaksandr on 29.08.2017.
 */

@Service
public class FileServiceImpl implements FileService {

    private static final Logger logger = Logger.getLogger(FileServiceImpl.class);

    @Autowired
    private FileDao fileDao;

    @Override
    @Transactional
    public File getFileById(Long id) {
        logger.info("getFileById: " + id);

        FileEntity fileEntity = fileDao.findById(Math.toIntExact(id));
        return new File(fileEntity.getLocation());
    }
}
