package com.gmail.eapbox.service;

import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.service.model.OrderDTO;

import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface OrderService {

    List<OrderDTO> getUserOrders();

    void changeOrderStatus(Integer orderNumber, OrderStatus orderStatus);
}
