package com.gmail.eapbox.service.util;

import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.repository.model.UserInformation;
import com.gmail.eapbox.service.model.UserDTO;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class UserConvert {

    private UserConvert() {
    }

    public static User convert(User user, UserDTO userDTO) {
        user.getUserInformation().setName(userDTO.getName());
        user.getUserInformation().setSurname(userDTO.getSurname());
        user.getUserInformation().setFathername(userDTO.getFathername());
        user.getUserInformation().setPhone(userDTO.getPhone());
        user.getUserInformation().setAddress(userDTO.getAddress());
        user.getUserInformation().setAddInfo(userDTO.getAddInfo());
        return user;
    }

    public static User convert(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        user.setBlocked(userDTO.getBlocked());

        UserInformation userInformation = new UserInformation();
        userInformation.setName(userDTO.getName());
        userInformation.setSurname(userDTO.getSurname());
        userInformation.setFathername(userDTO.getFathername());
        userInformation.setPhone(userDTO.getPhone());
        userInformation.setAddress(userDTO.getAddress());
        userInformation.setAddInfo(userDTO.getAddInfo());

        user.setUserInformation(userInformation);
        return user;
    }

    public static UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        userDTO.setBlocked(user.getBlocked());

        userDTO.setName(user.getUserInformation().getName());
        userDTO.setSurname(user.getUserInformation().getSurname());
        userDTO.setFathername(user.getUserInformation().getFathername());
        userDTO.setPhone(user.getUserInformation().getPhone());
        userDTO.setAddress(user.getUserInformation().getAddress());
        userDTO.setAddInfo(user.getUserInformation().getAddInfo());

        return userDTO;
    }
}
