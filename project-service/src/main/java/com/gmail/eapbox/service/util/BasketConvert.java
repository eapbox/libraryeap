package com.gmail.eapbox.service.util;


import com.gmail.eapbox.repository.model.Order;
import com.gmail.eapbox.service.model.BasketDTO;

import java.math.BigDecimal;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class BasketConvert {

    private BasketConvert() {
    }

    public static Order convert(BasketDTO basketDTO) {
        Order order = new Order();
        order.setDate(basketDTO.getDate());
        order.setAmount(basketDTO.getGoodAmount());
        order.setOrderStatus(basketDTO.getOrderStatus());
        order.setOrderNumber(basketDTO.getOrderNumber());
        return order;
    }

    public static BasketDTO convert(Order order) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setId(order.getId());
        basketDTO.setUserEmail(order.getUser().getEmail());
        basketDTO.setOrderStatus(order.getOrderStatus());
        basketDTO.setOrderNumber(order.getOrderNumber());
        basketDTO.setGoodInvNumber(order.getGood().getInvNumber());
        basketDTO.setGoodAmount(order.getAmount());
        basketDTO.setGoodName(order.getGood().getName());
        basketDTO.setGoodInfo(order.getGood().getInfo());
        basketDTO.setGoodPrice(order.getGood().getPrice());
        basketDTO.setTotalSum(basketDTO.getGoodPrice().multiply(new BigDecimal(basketDTO.getGoodAmount())));
        return basketDTO;
    }
}
