package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.UserDao;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Service(value = "appUserDetailsService")
public class AppUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {
        User user = userDao.getUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        AppUserPrincipal appUserPrincipal = new AppUserPrincipal(user);

        return appUserPrincipal;
    }
}
