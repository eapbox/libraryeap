package com.gmail.eapbox.service.model;

import com.gmail.eapbox.repository.model.OrderStatus;

import java.math.BigDecimal;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class BasketDTO {

    private Integer id;
    private String date;
    private String userEmail;
    private OrderStatus orderStatus;
    private Integer orderNumber;
    private Integer goodInvNumber;
    private Integer goodAmount;
    private String goodName;
    private String goodInfo;
    private BigDecimal goodPrice;
    private BigDecimal totalSum;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getGoodInvNumber() {
        return goodInvNumber;
    }

    public void setGoodInvNumber(Integer goodInvNumber) {
        this.goodInvNumber = goodInvNumber;
    }

    public Integer getGoodAmount() {
        return goodAmount;
    }

    public void setGoodAmount(Integer goodAmount) {
        this.goodAmount = goodAmount;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodInfo() {
        return goodInfo;
    }

    public void setGoodInfo(String goodInfo) {
        this.goodInfo = goodInfo;
    }

    public BigDecimal getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(BigDecimal goodPrice) {
        this.goodPrice = goodPrice;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }
}
