package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.UserDao;
import com.gmail.eapbox.repository.UserInformationDao;
import com.gmail.eapbox.repository.impl.UserDaoImpl;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.repository.model.UserInformation;
import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.UserService;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import com.gmail.eapbox.service.model.UserDTO;
import com.gmail.eapbox.service.util.CurrentUser;
import com.gmail.eapbox.service.util.UserConvert;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Service
public class UserServiceImpl implements UserService{
    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public List<UserDTO> getAll() {
        logger.info("getAll");
        List<User> users = userDao.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : users) {
            userDTOList.add(UserConvert.convert(user));
        }
        return userDTOList;
    }

    @Override
    @Transactional
    public UserDTO getCurrentUser() {
        logger.info("getCurrentUser");
        User user = CurrentUser.getCurrentUser();
        UserDTO userDTO = UserConvert.convert(user);
        logger.info("CurrentUser: " + userDTO.getEmail());
        return userDTO;
    }

    @Override
    @Transactional()
    public void save(UserDTO userDTO) {
        logger.info("saveUser: " + userDTO.getEmail());
        User user = UserConvert.convert(userDTO);
        user.setRole(UserRole.ROLE_USER);
        user.setBlocked(false);
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDao.save(user);
    }

    @Override
    @Transactional()
    public void updateCurrentUser(UserDTO userDTO) {
        logger.info("updateUser: " + userDTO.getEmail());
        User user = UserConvert.convert(CurrentUser.getCurrentUser(), userDTO);
        if (userDTO.getPassword().length() > 0 ) {
            user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        }
        userDao.saveOrUpdate(user);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        logger.info("deleteNews id=" + id);
        User user = userDao.findById(id);
        userDao.delete(user);
    }

    @Override
    @Transactional
    public void changeBlocked(Integer id, Boolean blocked) {
        logger.info("changeBlocked id=" + id);
        userDao.changeBlocked(id, blocked);
    }

    @Override
    @Transactional
    public void changeRole(Integer id, UserRole role) {
        logger.info("changeRole id=" + id);
        userDao.changeRole(id, role);
    }
}
