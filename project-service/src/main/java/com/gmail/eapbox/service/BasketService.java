package com.gmail.eapbox.service;



import com.gmail.eapbox.service.model.BasketInfoDTO;
import com.gmail.eapbox.service.model.BasketDTO;

import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface BasketService {

    BasketInfoDTO getBasketInfo();

    List<BasketDTO> getCurentBasket();

    void save(BasketDTO basketDTO);

    void setAmount(Integer id, Integer amount);

    void basketToOrder();
}
