package com.gmail.eapbox.service.model;

import com.gmail.eapbox.repository.model.OrderStatus;

import java.math.BigDecimal;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class OrderDTO {

    private Integer orderNumber;
    private String date;
    private Integer goodAmount;
    private BigDecimal orderSum;
    private String username;
    private OrderStatus orderStatus;

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getGoodAmount() {
        return goodAmount;
    }

    public void setGoodAmount(Integer goodAmount) {
        this.goodAmount = goodAmount;
    }

    public BigDecimal getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(BigDecimal orderSum) {
        this.orderSum = orderSum;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}
