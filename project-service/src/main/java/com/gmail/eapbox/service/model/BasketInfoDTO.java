package com.gmail.eapbox.service.model;

import java.math.BigDecimal;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class BasketInfoDTO {
    private BigDecimal basketSum;
    private Integer basketPosition;

    public BasketInfoDTO() {
        this.basketSum = BigDecimal.valueOf(0);
        this.basketPosition = 0;
    }

    public BigDecimal getBasketSum() {
        return basketSum;
    }

    public void setBasketSum(BigDecimal basketSum) {
        this.basketSum = basketSum;
    }

    public Integer getBasketPosition() {
        return basketPosition;
    }

    public void setBasketPosition(Integer basketPosition) {
        this.basketPosition = basketPosition;
    }
}
