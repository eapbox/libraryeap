package com.gmail.eapbox.service;


import com.gmail.eapbox.service.model.GoodDTO;

import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface GoodService {

    List<GoodDTO> getAll();

    GoodDTO getById(Integer id);

    GoodDTO getByInvNumber(Integer invNumber);

    void save(GoodDTO goodDTO);

    void update(GoodDTO goodDTO);

    void addToBasket(Integer id);
}
