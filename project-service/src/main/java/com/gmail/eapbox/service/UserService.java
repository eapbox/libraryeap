package com.gmail.eapbox.service;

import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.model.UserDTO;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface UserService {

    List<UserDTO> getAll();

    UserDTO getCurrentUser();

    void save(UserDTO userDTO);

    void updateCurrentUser(UserDTO userDTO);

    void delete(Integer id);

    void changeBlocked(Integer id, Boolean blocked);

    void changeRole(Integer id, UserRole role);
}
