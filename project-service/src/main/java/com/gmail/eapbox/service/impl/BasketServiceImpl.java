package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.OrderDao;
import com.gmail.eapbox.repository.model.Order;
import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.service.BasketService;
import com.gmail.eapbox.service.model.BasketInfoDTO;
import com.gmail.eapbox.service.model.BasketDTO;
import com.gmail.eapbox.service.util.CurrentUser;
import com.gmail.eapbox.service.util.BasketConvert;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Service
public class BasketServiceImpl implements BasketService {
    private static final Logger logger = Logger.getLogger(BasketServiceImpl.class);

    @Autowired
    OrderDao orderDao;

    @Override
    @Transactional
    public BasketInfoDTO getBasketInfo() {
        logger.info("getBasketInfo");

        BasketInfoDTO basketInfoDTO = new BasketInfoDTO();
        List<Order> basket = orderDao.getBasket(CurrentUser.getCurrentUser().getEmail());
        for (Order order : basket) {
            basketInfoDTO.setBasketSum(
                basketInfoDTO.getBasketSum().add(
                order.getGood().getPrice().
                multiply(BigDecimal.valueOf(order.getAmount())))
            );
            basketInfoDTO.setBasketPosition(basketInfoDTO.getBasketPosition()  + 1);
        }
        return basketInfoDTO;
    }

    @Override
    @Transactional
    public List<BasketDTO> getCurentBasket() {
        logger.info("getCurentBasket");

        List<Order> basket = orderDao.getBasket(CurrentUser.getCurrentUser().getEmail());
        List<BasketDTO> basketDTOList = new ArrayList<>();
        for (Order order : basket) {
            BasketDTO basketDTO = BasketConvert.convert(order);
            basketDTOList.add(basketDTO);
        }
        return basketDTOList;
    }

    @Override
    @Transactional
    public void save(BasketDTO basketDTO) {
        logger.info("saveOrder: " + basketDTO.getGoodInvNumber());

        Order order = BasketConvert.convert(basketDTO);
        orderDao.save(order);
    }

    @Override
    @Transactional
    public void setAmount(Integer id, Integer amount) {
        logger.info("setAmount id:" + id + "; amount=" + amount);

        if (amount > 0) {
            orderDao.setAmountGood(id, amount);
        } else {
            orderDao.deleteById(id);
        }
    }

    @Override
    @Transactional
    public void basketToOrder() {
        logger.info("basketToOrder");

        orderDao.setStatus(OrderStatus.NEW, CurrentUser.getCurrentUser());
    }
}
