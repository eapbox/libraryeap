package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.GoodDao;
import com.gmail.eapbox.repository.OrderDao;
import com.gmail.eapbox.repository.UserDao;
import com.gmail.eapbox.repository.model.Good;
import com.gmail.eapbox.repository.model.Order;
import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.service.GoodService;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import com.gmail.eapbox.service.model.GoodDTO;
import com.gmail.eapbox.service.util.CurrentUser;
import com.gmail.eapbox.service.util.GoodConvert;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Service
public class GoodServiceImpl implements GoodService {
    private static final Logger logger = Logger.getLogger(GoodServiceImpl.class);

    private final GoodDao goodDao;
    private final OrderDao orderDao;

    @Autowired
    public GoodServiceImpl(GoodDao goodDao, OrderDao orderDao) {
        this.goodDao = goodDao;
        this.orderDao = orderDao;
    }

    @Override
    @Transactional
    public List<GoodDTO> getAll() {
        logger.info("findAll - start");

        List<Good> goodList = goodDao.findAll();
        List<GoodDTO> goodDTOList = new ArrayList<>();
        for (Good good : goodList) {
            GoodDTO goodDTO = GoodConvert.convert(good);
            goodDTOList.add(goodDTO);
        }
        return goodDTOList;
    }

    @Override
    @Transactional
    public GoodDTO getById(Integer id) {
        logger.info("getById - start");

        Good good = goodDao.findById(id);
        GoodDTO goodDTO = GoodConvert.convert(good);
        return good != null ? goodDTO : null;
    }

    @Override
    @Transactional
    public GoodDTO getByInvNumber(Integer invNumber) {
        logger.info("getByInvNumber: " + invNumber);

        Good good = goodDao.getByInvNumber(invNumber);
        return good != null ? GoodConvert.convert(good) : null;
    }

    @Override
    @Transactional
    public void save(GoodDTO goodDTO) {
        logger.info("saveGood: " + goodDTO.getInvNumber());

        goodDTO.setBlocked(false);
        Good good = GoodConvert.convert(goodDTO);
        goodDao.save(good);
    }

    @Override
    @Transactional
    public void update(GoodDTO goodDTO) {
        logger.info("updateGood: " + goodDTO.getInvNumber());

        Good good = GoodConvert.convert(goodDao.findById(goodDTO.getId()), goodDTO);
        goodDao.saveOrUpdate(good);
    }

    @Override
    @Transactional
    public void addToBasket(Integer goodId) {
        logger.info("addToBasket id=" + goodId);

        Integer orderNumber = orderDao.getCurrentOrderNumber(CurrentUser.getCurrentUser().getEmail());
        Good good = goodDao.findById(goodId);
        Order existOrder = orderDao.getOrderBasketByGood(good);
        if (existOrder != null) {
            orderDao.setAmountGood(existOrder.getId(), existOrder.getAmount() +1);
        } else if (existOrder == null) {
            Order order = new Order();
            User user = CurrentUser.getCurrentUser();
            order.setUser(user);
            good.setBlocked(true);
            order.setGood(good);
            order.setOrderStatus(OrderStatus.BASKET);
            order.setDate((new Date()).toString());
            order.setOrderNumber(orderNumber);
            order.setAmount(1);
            orderDao.save(order);
        }
    }
}
