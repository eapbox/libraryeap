package com.gmail.eapbox.service;

import java.io.File;

/**
 * Created by Ershov Aliaksandr on 29.08.2017.
 */
public interface FileService {

    File getFileById(Long id);

}
