package com.gmail.eapbox.service;

import com.gmail.eapbox.service.model.NewsDTO;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public interface NewsService {

    List<NewsDTO> getAll();

    NewsDTO getById(Integer id);

    void save(NewsDTO newsDTO) throws IOException;

    void update(NewsDTO newsDTO) throws IOException;

    void delete(Integer id) throws IOException;
}
