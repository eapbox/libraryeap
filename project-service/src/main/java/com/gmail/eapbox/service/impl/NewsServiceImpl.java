package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.NewsDao;
import com.gmail.eapbox.repository.UserDao;
import com.gmail.eapbox.repository.model.FileEntity;
import com.gmail.eapbox.repository.model.News;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.service.NewsService;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import com.gmail.eapbox.service.model.NewsDTO;
import com.gmail.eapbox.service.util.CurrentUser;
import com.gmail.eapbox.service.util.NewsConvert;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Service
public class NewsServiceImpl implements NewsService {
    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);

    private final NewsDao newsDao;
    private Properties properties;

    @Autowired
    public NewsServiceImpl(NewsDao newsDao, Properties properties) {
        this.newsDao = newsDao;
        this.properties = properties;
    }

    @Override
    @Transactional
    public List<NewsDTO> getAll() {
        logger.info("findAll - start");

        List<News> newsList = newsDao.findAll();
        List<NewsDTO> newsDTOList = new ArrayList<>();
        for (News news : newsList) {
            NewsDTO newsDTO = NewsConvert.convert(news);
            newsDTOList.add(newsDTO);
        }
        return newsDTOList;
    }

    @Override
    @Transactional
    public NewsDTO getById(Integer id) {
        logger.info("getById: " + id);

        News news = newsDao.findById(id);
        NewsDTO newsDTO = NewsConvert.convert(news);
        return news != null ? newsDTO : null;
    }

    @Override
    @Transactional
    public void save(NewsDTO newsDTO) throws IOException {
        logger.info("saveNews: " + newsDTO.getCaption());

        Date date = new Date();
        newsDTO.setDate(date.toString());
        News news = NewsConvert.convert(newsDTO);
        try {
            FileEntity fileEntity = getFileEntity(newsDTO);
            news.setFileEntity(fileEntity);
            User user = CurrentUser.getCurrentUser();
            news.setUser(user);
            newsDao.save(news);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    @Transactional
    public void update(NewsDTO newsDTO) throws IOException{
        logger.info("updateNews: " + newsDTO.getId());

        News news = NewsConvert.convert(newsDao.findById(newsDTO.getId()), newsDTO);
        try {
            FileEntity fileEntity = getFileEntity(newsDTO);
            news.getFileEntity().setLocation(fileEntity.getLocation());
            news.getFileEntity().setFilename(fileEntity.getFilename());
            newsDao.saveOrUpdate(news);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        logger.info("deleteNews id=" + id);

        if (id > 0) {
            News news = newsDao.findById(id);
            newsDao.delete(news);
        }
    }

    private FileEntity getFileEntity(NewsDTO newsDTO) throws IOException {
        String filename = System.currentTimeMillis() + ".jpg";
        String fileLocation = properties.getProperty("upload.location") + filename;
        FileCopyUtils.copy(newsDTO.getFile().getBytes(), new File(fileLocation));
        FileEntity fileEntity = new FileEntity();
        fileEntity.setLocation(fileLocation);
        fileEntity.setFilename(newsDTO.getFile().getName());

        return fileEntity;
    }
}
