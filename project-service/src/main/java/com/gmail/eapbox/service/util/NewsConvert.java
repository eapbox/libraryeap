package com.gmail.eapbox.service.util;


import com.gmail.eapbox.repository.model.News;
import com.gmail.eapbox.service.model.NewsDTO;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class NewsConvert {

    private NewsConvert() {
    }

    public static News convert(News news, NewsDTO newsDTO) {
        news.setCaption(newsDTO.getCaption());
        news.setContent(newsDTO.getContent());

        return news;
    }

    public static News convert(NewsDTO newsDTO) {
        News news = new News();
        news.setCaption(newsDTO.getCaption());
        news.setContent(newsDTO.getContent());
        news.setDate(newsDTO.getDate());
        return news;
    }

    public static NewsDTO convert(News news) {
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setCaption(news.getCaption());
        newsDTO.setContent(news.getContent());
        newsDTO.setDate(news.getDate());
        newsDTO.setUsername(
            news.getUser().getUserInformation().getName() + " " +
                news.getUser().getUserInformation().getFathername() + " " +
                news.getUser().getUserInformation().getSurname());
        newsDTO.setId(news.getId());
        newsDTO.setFilename(news.getFileEntity().getFilename());
        newsDTO.setFilelocation(news.getFileEntity().getLocation());
        return newsDTO;
    }
}
