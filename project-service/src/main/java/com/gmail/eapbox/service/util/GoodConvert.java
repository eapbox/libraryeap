package com.gmail.eapbox.service.util;

import com.gmail.eapbox.repository.model.Good;
import com.gmail.eapbox.service.model.GoodDTO;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class GoodConvert {

    private GoodConvert() {
    }

    public static Good convert(Good good, GoodDTO goodDTO) {
        good.setName(goodDTO.getName());
        good.setInfo(goodDTO.getInfo());
        good.setPrice(goodDTO.getPrice());
        return good;
    }

    public static Good convert(GoodDTO goodDTO) {
        Good good = new Good();
        good.setId(goodDTO.getId());
        good.setInfo(goodDTO.getInfo());
        good.setInvNumber(goodDTO.getInvNumber());
        good.setBlocked(goodDTO.getBlocked());
        good.setName(goodDTO.getName());
        good.setPrice(goodDTO.getPrice());
        return good;
    }

    public static GoodDTO convert(Good good) {
        GoodDTO goodDTO = new GoodDTO();
        goodDTO.setId(good.getId());
        goodDTO.setInfo(good.getInfo());
        goodDTO.setInvNumber(good.getInvNumber());
        goodDTO.setBlocked(good.getBlocked());
        goodDTO.setName(good.getName());
        goodDTO.setPrice(good.getPrice());
        return goodDTO;
    }
}
