package com.gmail.eapbox.service.util;

import com.gmail.eapbox.repository.UserDao;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.service.model.AppUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

public class CurrentUser {

    public static User getCurrentUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
            .getAuthentication()
            .getPrincipal();

        return principal.getCurrentUser();
    }
}
