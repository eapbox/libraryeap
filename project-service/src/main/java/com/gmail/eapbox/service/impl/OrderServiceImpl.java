package com.gmail.eapbox.service.impl;

import com.gmail.eapbox.repository.OrderDao;
import com.gmail.eapbox.repository.model.Order;
import com.gmail.eapbox.repository.model.OrderStatus;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.repository.model.UserRole;
import com.gmail.eapbox.service.OrderService;
import com.gmail.eapbox.service.model.BasketDTO;
import com.gmail.eapbox.service.model.OrderDTO;
import com.gmail.eapbox.service.util.CurrentUser;
import com.gmail.eapbox.service.util.BasketConvert;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ershov Aliaksandr on xx.08.2017.
 */

@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger logger = Logger.getLogger(OrderServiceImpl.class);

    @Autowired
    OrderDao orderDao;

    @Override
    @Transactional
    public List<OrderDTO> getUserOrders() {
        logger.info("getUserOrders");

        User user = CurrentUser.getCurrentUser();
        List<Order> orderList = new ArrayList<>();
        if (user.getRole() == UserRole.ROLE_USER) {
            orderList = orderDao.getUserOrders(user.getEmail());

        } else
        if (user.getRole() == UserRole.ROLE_ROOT || user.getRole() == UserRole.ROLE_ADMIN) {
            orderList = orderDao.getAllOrders();
        }
        List<BasketDTO> basketDTOList = getBasketDTO(orderList);
        return getOrder(basketDTOList);
    }

    @Override
    @Transactional
    public void changeOrderStatus(Integer orderNumber, OrderStatus orderStatus) {
        logger.info("changeOrderStatus: " + orderNumber + "; new Status=" + orderStatus);

        orderDao.changeOrderStatus(orderNumber, orderStatus);
    }

    private List<BasketDTO> getBasketDTO(List<Order> orderList) {
        List<BasketDTO> basketDTOList = new ArrayList<>();
        for (Order order : orderList) {
            BasketDTO basketDTO = BasketConvert.convert(order);
            basketDTOList.add(basketDTO);
        }
        return basketDTOList;
    }

    private List<OrderDTO> getOrder(List<BasketDTO> basketDTOList) {
        List<OrderDTO> orderDTOList = new ArrayList<>();
        for (BasketDTO basket : basketDTOList) {
            int i;
            for (i = 0; i < orderDTOList.size(); i++) {
                if (orderDTOList.get(i).getOrderNumber() == basket.getOrderNumber()) {
                    BigDecimal sumBasketPosition =
                        basket.getGoodPrice()
                            .multiply(new BigDecimal(basket.getGoodAmount()));
                    BigDecimal price = orderDTOList.get(i).getOrderSum().add(sumBasketPosition);
                    orderDTOList.get(i).setOrderSum(price);
                    orderDTOList.get(i).setGoodAmount(
                        orderDTOList.get(i).getGoodAmount() + basket.getGoodAmount()
                    );
                    break;
                }
            }
            if (i == orderDTOList.size())
                orderDTOList.add(setBasketToOrder(basket));
        }
        return orderDTOList;
    }

    private OrderDTO setBasketToOrder(BasketDTO basketDTO) {
        OrderDTO orderDTO = new OrderDTO();

        orderDTO.setOrderNumber(basketDTO.getOrderNumber());
        orderDTO.setGoodAmount(basketDTO.getGoodAmount());
        orderDTO.setDate(basketDTO.getDate());
        orderDTO.setOrderStatus(basketDTO.getOrderStatus());
        orderDTO.setOrderSum(basketDTO.getGoodPrice()
            .multiply(new BigDecimal(basketDTO.getGoodAmount())));
        orderDTO.setUsername(basketDTO.getUserEmail());
        return orderDTO;
    }
}
